import os, sys, glob
from utilities import *

inFilePathPattern = sys.argv[1]
classFilePath = sys.argv[2]
outFilePath = sys.argv[3]

geneSetDataDict = {}
sampleIDs = set()
for inFilePath in glob.glob(inFilePathPattern):
    geneSet = os.path.basename(inFilePath)

    geneSetDataDict[geneSet] = {}
    for line in file(inFilePath):
        lineItems = line.rstrip().split("\t")

        sampleID = lineItems[0]
        sampleIDs.add(sampleID)

        geneSetDataDict[geneSet][sampleID] = lineItems[1]

sampleIDs = naturalSort(list(sampleIDs))

sampleClassDict = {}
for line in file(classFilePath):
    lineItems = line.rstrip().split("\t")
    sampleClassDict[lineItems[0]] = lineItems[1]

outFile = open(outFilePath, 'w')
outFile.write("\t".join(["SampleID"] + sampleIDs) + "\n")
outFile.write("\t".join(["SampleClass"] + [sampleClassDict[sampleID] for sampleID in sampleIDs]) + "\n")
for geneSet in naturalSort(geneSetDataDict.keys()):
    outFile.write("\t".join([geneSet] + [geneSetDataDict[geneSet][sampleID] for sampleID in sampleIDs]) + "\n")
outFile.close()
