#source("http://bioconductor.org/biocLite.R")
#biocLite("gage")
library(gage)

gmtFilePath = commandArgs()[7]
dataFilePath = commandArgs()[8]
classesFilePath = commandArgs()[9]
experimentType = commandArgs()[10]
outFilePath1 = commandArgs()[11]
outFilePath2 = commandArgs()[12]

classes <- read.table(classesFilePath, sep="\t", stringsAsFactors=F, header=F, row.names=1, check.names=F)
data <- as.matrix(read.table(dataFilePath, sep="\t", stringsAsFactors=F, header=T, row.names=1, check.names=F))
geneSets = readList(gmtFilePath)
  
if (experimentType == "Main") {
  class0 = (1:50)
  class1 = (51:100)
} else {
  class0 = (1:10)
  class1 = (11:100)
}

# Call GAGE using Fold Change
GAGE_FC <- gage(data, gsets = geneSets, ref = class0, samp = class1, compare = 'paired', same.dir = FALSE)
write.table(GAGE_FC$greater[,1:5], file = outFilePath1, sep = "\t", quote=F, col.names = NA)

# Call GAGE using t stastics
GAGE_tstats <- gage(data, gsets = geneSets, ref = class0, samp = class1, compare = 'paired', same.dir = FALSE, use.fold = FALSE)
write.table(GAGE_tstats$greater, file = outFilePath2, sep = "\t", quote=F, col.names = NA)
