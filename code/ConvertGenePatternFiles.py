import os, sys, glob, random
from utilities import *

inGctFilePath = sys.argv[1]
inClsFilePath = sys.argv[2]
outDataFilePath = sys.argv[3]
outClassesFilePath = sys.argv[4]

outClassesFile = open(outClassesFilePath, 'w')
outDataFile = open(outDataFilePath, 'w')

inGctFile = open(inGctFilePath, 'rU')
inGctFile.readline() # Get rid of pointless first header line
inGctFile.readline() # Get rid of pointless second header line

samples = inGctFile.readline().rstrip().split("\t")[2:]
outDataFile.write("\t".join([""] + samples) + "\n")

for line in inGctFile:
    lineItems = line.rstrip().split("\t")
    outDataFile.write("\t".join([lineItems[0]] + lineItems[2:]) + "\n")

inGctFile.close()

inClsFile = open(inClsFilePath, 'rU')

x = inClsFile.readline() # Get rid of pointless header
classNames = inClsFile.readline().rstrip().split(" ")[1:]
classIndices = [int(x) for x in inClsFile.readline().rstrip().split(" ")]
inClsFile.close()

if len(classIndices) != len(samples):
    print "The number of class values [%i] in %s does not match what's in %s [%i]." % (len(classIndices), inClsFilePath, inGctFilePath, len(samples))
    exit(1)

for i in range(len(samples)):
    outClassesFile.write("%s\t%s\n" % (samples[i], classNames[int(classIndices[i])]))

outDataFile.close()
outClassesFile.close()
