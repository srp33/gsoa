import os, sys, glob, random
from utilities import *

gmtFilePaths = sys.argv[1].split(",")
classesFilePath = sys.argv[2]
inDataFilePaths = sys.argv[3]
tmpDirPath = sys.argv[4]
numRandomIterations = int(sys.argv[5])
rowsToExclude = sys.argv[6]
outMainDataDirPath = sys.argv[7]
outMainResultDirPath = sys.argv[8]
outMainPredictionsDirPath = sys.argv[9]
outRandomDataDirPath = sys.argv[10]
outRandomResultDirPath = sys.argv[11]
outRandomPredictionsDirPath = sys.argv[12]
commandPrefix = sys.argv[13]
outCommandFilePath = sys.argv[14]
numFolds = sys.argv[15]
classificationAlgorithm = sys.argv[16]
permuteSeed = sys.argv[17]
outTargetClassFilePath = sys.argv[18]

if rowsToExclude == "":
    rowsToExclude = set()
else:
    rowsToExclude = set(rowsToExclude.split(","))

def saveGeneSetData(geneSet, description, geneSetData, outDataDirPath, outResultDirPath, outPredictionsDirPath):
    if len(geneSetData) == 0:
        print "No data to save for %s, %s" % (description, geneSet)
        return

    outputText = ""
    outputText += "\t".join([sample for sample in samples]) + "\n"
    for item in geneSetData:
        outputText += "\t".join([str(x) for x in item]) + "\n"
    outputText += "\t".join([classDict[sample] for sample in samples]) + "\n"

    print "Saving data for %s, %s" % (description, geneSet)
    outFilePath = outDataDirPath + "/" + geneSet
    outFile = open(outFilePath, 'w')
    outFile.write(outputText)
    outFile.close()

    commands.append("%s '%s' '%s' '%s/%s' '%s/%s' '%s' '%s' '%s'" % (commandPrefix, description, outFilePath, outResultDirPath, geneSet, outPredictionsDirPath, geneSet, numFolds, classificationAlgorithm, targetClass))



geneSetDict = {}
geneSetUniqueGenes = set()

for gmtFilePattern in gmtFilePaths:
    for gmtFilePath in glob.glob(gmtFilePattern):
        for line in file(gmtFilePath):
            lineItems = line.rstrip().split("\t")
            geneSet = lineItems[0]
            genes = [x.upper() for x in lineItems[2:]]

            geneSetDict[geneSet] = genes
            geneSetUniqueGenes.update(genes)

if len(geneSetDict) == 0:
    print "No gene sets could be found."
    exit(1)

# Parse input data file paths (can be separated by commas and/or wildcards can be used)
inDataFilePathList = []
for x in inDataFilePaths.split(","):
    inDataFilePathList.extend(glob.glob(x))

# Convert .gct files to standard format
for i in range(len(inDataFilePathList)):
    if inDataFilePathList[i].endswith(".gct"):
        newDataFilePath = tmpDirPath + "/" + os.path.basename(inDataFilePathList[i])
        newClassesFilePath = tmpDirPath + "/" + os.path.basename(classesFilePath)

        convertGenePatternFiles(inDataFilePathList[i], classesFilePath, newDataFilePath, newClassesFilePath)

        inDataFilePathList[i] = newDataFilePath
        classesFilePath = newClassesFilePath

if len(inDataFilePathList) == 1:
    inDataFilePath = inDataFilePathList[0]
else:
    inDataFilePath = tmpDirPath + "/MergedData.txt"
    mergeDataFiles(inDataFilePathList, inDataFilePath)

allClassDict = {}

for line in file(classesFilePath):
    lineItems = line.rstrip().split("\t")
    sampleID = lineItems[0]
    sampleClass = lineItems[1]
    allClassDict[sampleID] = sampleClass

targetClass = sorted(list(set(allClassDict.values())))[0]

outTargetClassFile = open(outTargetClassFilePath, 'w')
outTargetClassFile.write(targetClass)
outTargetClassFile.close()

if len(allClassDict) == 0:
    print "No class information could be found."
    exit(1)

# In the future, we may support multiple classes.
if len(set(allClassDict.values())) != 2:
    print "This tool currently only supports analyses for two classes."
    exit(1)

inDataFile = open(inDataFilePath)

samples = inDataFile.readline().rstrip().split("\t")[1:]
uniqueDataSamples = set(samples)

classDict = {}
for sample in samples:
    if sample in allClassDict:
        classDict[sample] = allClassDict[sample]

if permuteSeed != "":
    print "Permuting class labels using random seed %s" % permuteSeed
    permutedClassDict = {}

    randomClassDictKeys = list(classDict.keys())
    randomClassDictValues = list(classDict.values())
    random.seed(int(permuteSeed))
    random.shuffle(randomClassDictKeys)

    for i in range(len(randomClassDictKeys)):
        classDict[randomClassDictKeys[i]] = randomClassDictValues[i]

# Count the number of samples in each class
classSampleCountDict = {}
for sample in samples:
    if sample in classDict:
        classSampleCountDict[classDict[sample]] = classSampleCountDict.setdefault(classDict[sample], 0) + 1

# Remove any class that does not have an adequate number of samples
for sampleClass in classSampleCountDict:
    if numFolds != "n" and classSampleCountDict[sampleClass] < int(numFolds):
        print "Only %i samples of class %s exist in the data, so leave-one-out cross validation will be used." % (classSampleCountDict[sampleClass], sampleClass)
        numFolds = "n"

sampleIndices = [i for i in range(len(samples)) if samples[i] in classDict.keys()]
samples = [x for x in samples if x in classDict.keys()] # keeps order of samples

duplicateSamples = [x for x in samples if samples.count(x) > 1]

if len(duplicateSamples) > 0:
    print "This analysis cannot be performed because the following samples are duplicates: %s." % ",".join(duplicateSamples)
    exit(1)

if len(samples) == 0:
    print "No samples (columns) in the data file contained class information."
    exit(1)
else:
    print "%i samples (columns) in the data file contained class information." % len(sampleIndices)

# Build a dict that contains data for each gene
numDataRows = 0
numDataRowsInGeneSets = 0
dataDict = {}
for line in inDataFile:
    lineItems = line.rstrip().split("\t")
    gene = lineItems.pop(0).upper()
    values = [lineItems[i] for i in sampleIndices]

    if len(set(["null", "nan"]) & set(values)) > 0:
        continue

    if gene in rowsToExclude:
        print "Excluding %s as requested" % gene
        continue

    # Exclude genes for which all values are identical
    if len(set(values)) <= 1:
        continue

    if gene in geneSetUniqueGenes:
        numDataRowsInGeneSets += 1
    numDataRows += 1

    if gene not in dataDict:
        dataDict[gene] = []
    dataDict[gene].append(values) # Genes with multiple rows will have multiple entries

inDataFile.close()

# Select only genes for which there are data
for geneSet in geneSetDict.keys():
    geneSetDict[geneSet] = list(set(geneSetDict[geneSet]) & set(dataDict.keys()))

commands = []

# Save data for each gene set
geneSetLengths = set()
### Parallelize here using multiprocessing package?
for geneSet in sorted(geneSetDict.keys()):
    geneSetData = []

    for gene in geneSetDict[geneSet]:
        if gene in dataDict.keys():
            for values in dataDict[gene]:
                geneSetData.append(values)

    if len(geneSetData) > 0:
        saveGeneSetData(geneSet, "main", geneSetData, outMainDataDirPath, outMainResultDirPath, outMainPredictionsDirPath)
        geneSetLengths.add(len(geneSetData))

bins = findNumGeneBins(geneSetLengths)

print "%i genes had data for all samples." % numDataRows
print "%i genes had data for all samples and belonged to at least one gene set." % numDataRowsInGeneSets
print "The following bins will be used for random selection: %s" % ", ".join([str(x) for x in bins])

# Create data list that will allow us to randomly pick data rows, even if genes have multiple values
dataList = []
for gene in dataDict.keys():
    for item in dataDict[gene]:
        dataList.append(item)
    del dataDict[gene] # To save memory

random.seed(1)
### Parallelize here using multiprocessing package?
for numGenes in bins:
    for i in range(numRandomIterations):
        randomGeneSet = "%iGenes___%i" % (numGenes, i)

        randomDataIndices = range(len(dataList))
        random.shuffle(randomDataIndices)
        randomGeneSetData = [dataList[i] for i in randomDataIndices[:numGenes]]

        saveGeneSetData(randomGeneSet, "random", randomGeneSetData, outRandomDataDirPath, outRandomResultDirPath, outRandomPredictionsDirPath)

print "%i commands will be executed." % len(commands)

outCommandFile = open(outCommandFilePath, 'w')
outCommandFile.write("set -o errexit\n")
outCommandFile.write("\n".join(commands) + "\n")
outCommandFile.close()
