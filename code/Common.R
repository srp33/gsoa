plotSimulatedResults = function(description, numSignal, metricValues, outFilePath, ylab)
{
  data = as.data.frame(cbind(numSignal, metricValues))
  data = data[order(data$metricValues),]

  xlab = "# Signal Genes"

  pdf(outFilePath, height=4.5, width=9)
  par(mar=c(4.6, 5.6, 0.6, 0.6))
  boxplot(data$metricValues ~ as.factor(data$numSignal), ylim=c(0, 1), xlab=xlab, ylab=ylab, main="", cex.axis=1.7, cex.lab=2.0, lwd=2)
  box(lwd=3)
  graphics.off()
}

calcAUC = function(numericValues, answers, direction="<")
{
  library(pROC)
  roc_result = roc(formula = as.factor(answers) ~ numericValues, ci=TRUE, plot=FALSE, print.auc=FALSE, main=markers, direction=direction)

  return(roc_result$ci[2])
}

plotROC = function(outFilePath, classValues, predictionValues)
{
  library(pROC)
  pdf(outFilePath)
  roc_result = roc(formula = as.factor(classValues) ~ predictionValues, ci=TRUE, plot=FALSE, print.auc=FALSE, main="", direction="auto")
  plot(1 - roc_result$specificities, roc_result$sensitivities, type="l", xlab="1 - Specificity", ylab="Sensitivity", lwd=3)
  abline(a=0, b=1, col="gray")
  auc = roc_result$ci[2]
  text(0.4, 0.4, paste("AUC: ", auc, sep=""))
  graphics.off()
}
