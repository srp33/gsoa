import os, sys, time

command = sys.argv[1]
outFilePath = sys.argv[2]

startTime = time.time()
os.system(command)
endTime = time.time()

outFile = open(outFilePath, 'w')
outFile.write("%.4f\n" % (endTime - startTime))
outFile.close()

print "%.4f seconds elapsed" % (endTime - startTime)
