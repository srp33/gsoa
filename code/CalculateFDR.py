import os, sys, glob, math
import multicomp
import numpy
from utilities import *

inFilePath = sys.argv[1]
outFilePath = sys.argv[2]

inFile = open(inFilePath)
outFile = open(outFilePath, 'w')

outFile.write(inFile.readline().rstrip() + "\tFDR\n") # Header line

data = []
for line in inFile:
    data.append(line.rstrip().split("\t"))

pValues = [float(row[2]) for row in data]
fdrReject, fdrValues, alphacSidak, alphacBonf = multicomp.multipletests(numpy.array(pValues), alpha=0.05, method='fdr_bh', returnsorted=False)

for i in range(len(data)):
    outFile.write("\t".join(data[i]) + "\t%.6f\n" % fdrValues[i])

outFile.close()
inFile.close()
