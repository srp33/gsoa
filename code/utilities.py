import glob, os, posix, sys, math, collections, json, difflib
from operator import itemgetter, attrgetter
import itertools
from random import uniform, sample
from collections import defaultdict
from scipy.stats import *

def readScalarFromFile(filePath):
    return readMatrixFromFile(filePath)[0][0]

def writeScalarToFile(x, filePath):
    outFile = open(filePath, 'w')
    outFile.write(x)
    outFile.close()

def readVectorFromFile(filePath):
    return [line.rstrip() for line in file(filePath)]

def writeVectorToFile(data, filePath):
    outFile = open(filePath, 'w')
    for x in data:
        outFile.write(str(x) + "\n")
    outFile.close()

def readMatrixFromFile(filePath, numLines=None):
    matrix = []
    for line in file(filePath):
        if numLines != None and len(matrix) >= numLines:
            break

        matrix.append(line.rstrip().split("\t"))

        if len(matrix) % 100000 == 0:
            print len(matrix)

    return matrix

def writeMatrixToFile(x, filePath, writeMode='w'):
    outFile = open(filePath, writeMode)
    writeMatrixToOpenFile(x, outFile)
    outFile.close()

def writeMatrixToOpenFile(x, outFile):
    for y in x:
        outFile.write("\t".join([str(z) for z in y]) + "\n")

def appendMatrixToFile(x, filePath):
    writeMatrixToFile(x, filePath, writeMode='a')

def readTextFromFile(filePath):
    text = ""

    for line in file(filePath):
        text += line

    return text

def calculateMean(values):
    if len(values) == 0:
        return float('nan')

    return sum(values) / len(values)

def calculateStandardDeviation(values):
    xbar = calculateMean(values)
    residuals = [x - xbar for x in values]
    residualsSquared = [x**2 for x in residuals]
    return math.sqrt(sum(residualsSquared) / (len(values) - 1))

def sortMatrix(data, columnIndex, reverse=False):
    data.sort(key=itemgetter(columnIndex), reverse=reverse)
    return data

def findNumGeneBins(numGenesSet):
    binOptions = [0, 1, 5, 10, 25, 50, 75, 100, 125, 150, 200, 250, 300, 400, 500, 1000000]

    if len(numGenesSet) <= len(binOptions):
        return sorted(list(numGenesSet))

    bins = []

    for i in range(len(binOptions))[1:]:
        if len([x for x in numGenesSet if x > binOptions[i-1] and x <= binOptions[i]]) > 0:
            bins.append(binOptions[i])

    if binOptions[-1] in bins:
        bins[-1] = max(list(numGenesSet))

    return bins

def getBin(numGenes, bins, maxNumGenes):
    if numGenes in bins:
        return numGenes

    binOptions = [0, 1, 5, 10, 25, 50, 75, 100, 125, 150, 200, 250, 300, 400, 500, 1000000]

    for i in range(len(binOptions))[1:]:
        if numGenes > binOptions[i-1] and numGenes <= binOptions[i]:
            if i == (len(binOptions) - 1):
                return maxNumGenes
            return binOptions[i]

def getConfigValue(configFilePath, key):
    configDict = {}
    for line in file(configFilePath):
        if line.startswith("#"):
            continue

        lineItems = line.rstrip().split("=")
        if lineItems[0] == key:
            return lineItems[1]

    print "No %s key was found." % key
    return None

def calculateSpearmanCoefficient(xList, yList):
    return stats.spearmanr(xList, yList)[0]

# Copied from: http://code.activestate.com/recipes/491268-ordering-and-ranking-for-lists/
def rankSmart(x, NoneIsLast=True, decreasing = False, ties = "first"):
    """
    Returns the ranking of the elements of x. The position of the first
    element in the original vector is rank[0] in the sorted vector.

    Missing values are indicated by None.  Calls the order() function.
    Ties are NOT averaged by default. Choices are:
                 "first" "average" "min" "max" "random" "average"
    """
    omitNone = False
    if NoneIsLast == None:
        NoneIsLast = True
        omitNone = True
    O = order(x, NoneIsLast = NoneIsLast, decreasing = decreasing)
    R = O[:]
    n = len(O)
    for i in range(n):
        R[O[i]] = i
    if ties == "first" or ties not in ["first", "average", "min", "max", "random"]:
        return R

    blocks     = []
    isnewblock = True
    newblock   = []
    for i in range(1,n) :
        if x[O[i]] == x[O[i-1]]:
            if i-1 not in newblock:
                newblock.append(i-1)
            newblock.append(i)
        else:
            if len(newblock) > 0:
                blocks.append(newblock)
                newblock = []
    if len(newblock) > 0:
        blocks.append(newblock)

    for i, block  in enumerate(blocks):
        # Don't process blocks of None values.
        if x[O[block[0]]] == None:
            continue
        if ties == "average":
            s = 0.0
            for j in block:
                s += j
            s /= float(len(block))
            for j in block:
                R[O[j]] = s
        elif ties == "min":
            s = min(block)
            for j in block:
                R[O[j]] = s
        elif ties == "max":
            s =max(block)
            for j in block:
                R[O[j]] = s
        elif ties == "random":
            s = sample([O[i] for i in block], len(block))
            for i,j in enumerate(block):
                R[O[j]] = s[i]
        else:
            for i,j in enumerate(block):
                R[O[j]] = j
    if omitNone:
        R = [ R[j] for j in range(n) if x[j] != None]
    return R

# Copied from: http://code.activestate.com/recipes/491268-ordering-and-ranking-for-lists/
def order(x, NoneIsLast = True, decreasing = False):
    """
    Returns the ordering of the elements of x. The list
    [ x[j] for j in order(x) ] is a sorted version of x.

    Missing values in x are indicated by None. If NoneIsLast is true,
    then missing values are ordered to be at the end.
    Otherwise, they are ordered at the beginning.
    """
    omitNone = False
    if NoneIsLast == None:
        NoneIsLast = True
        omitNone = True

    n  = len(x)
    ix = range(n)
    if None not in x:
        ix.sort(reverse = decreasing, key = lambda j : x[j])
    else:
        # Handle None values properly.
        def key(i, x = x):
            elem = x[i]
            # Valid values are True or False only.
            if decreasing == NoneIsLast:
                return not(elem is None), elem
            else:
                return elem is None, elem
        ix = range(n)
        ix.sort(key=key, reverse=decreasing)

    if omitNone:
        n = len(x)
        for i in range(n-1, -1, -1):
            if x[ix[i]] == None:
                n -= 1
        return ix[:n]
    return ix

def transposeMatrix(x):
    transposed = zip(*x)

    for i in range(len(transposed)):
        transposed[i] = list(transposed[i])

    return transposed

## From http://stackoverflow.com/questions/34518/natural-sorting-algorithm
def naturalSort(x, reverse=False):
    def natural_key(s):
        return tuple(
            int(''.join(chars)) if isdigit else ''.join(chars)
            for isdigit, chars in itertools.groupby(s, str.isdigit)
        )

    return sorted(x, key=natural_key, reverse=reverse)

def convertGenePatternFiles(inGctFilePath, inClsFilePath, outDataFilePath, outClassesFilePath):
    outClassesFile = open(outClassesFilePath, 'w')
    outDataFile = open(outDataFilePath, 'w')

    inGctFile = open(inGctFilePath, 'rU')
    inGctFile.readline() # Get rid of pointless first header line
    inGctFile.readline() # Get rid of pointless second header line

    samples = inGctFile.readline().rstrip().split("\t")[2:]
    outDataFile.write("\t".join([""] + samples) + "\n")

    for line in inGctFile:
        lineItems = line.rstrip().split("\t")
        outDataFile.write("\t".join([lineItems[0]] + lineItems[2:]) + "\n")

    inGctFile.close()

    inClsFile = open(inClsFilePath, 'rU')

    x = inClsFile.readline() # Get rid of pointless header
    classNames = inClsFile.readline().rstrip().split(" ")[1:]
    classIndices = [int(x) for x in inClsFile.readline().rstrip().split(" ")]
    inClsFile.close()

    if len(classIndices) != len(samples):
        print "The number of class values [%i] in %s does not match what's in %s [%i]." % (len(classIndices), inClsFilePath, inGctFilePath, len(samples))
        exit(1)

    for i in range(len(samples)):
        outClassesFile.write("%s\t%s\n" % (samples[i], classNames[int(classIndices[i])]))

    outDataFile.close()
    outClassesFile.close()


def mergeDataFiles(inDataFilePathList, outFilePath):
    inDataFile = open(inDataFilePathList[0])
    commonSamples = set(inDataFile.readline().rstrip().split("\t")[1:])
    inDataFile.close()

    for inDataFilePath in inDataFilePathList[1:]:
        inDataFile = open(inDataFilePath)
        samples = set(inDataFile.readline().rstrip().split("\t")[1:])
        inDataFile.close()

        commonSamples = commonSamples & samples

    commonSamples = sorted(list(commonSamples))

    outFile = open(outFilePath, 'w')
    outFile.write("\t".join([""] + commonSamples) + "\n")

    for inDataFilePath in inDataFilePathList:
        inDataFile = open(inDataFilePath)
        headerItems = inDataFile.readline().rstrip().split("\t")

        colIndices = [0] + [headerItems.index(x) for x in commonSamples if x in headerItems]

        for line in inDataFile:
            lineItems = line.rstrip().split("\t")
            lineItems = [lineItems[i] for i in colIndices]
            outFile.write("\t".join(lineItems) + "\n")
        inDataFile.close()

    outFile.close()
