# dataFilePaths=Data/Simulated/Data.txt classFilePath=Data/Simulated/Classes2.txt gmtFilePaths=Data/Simulated/genesets.gmt numCores=57 numRandomIterations=100 rowsToExclude= numFolds=5 classificationAlgorithm=svmrbf permuteSeed=1 date=Tue Dec 30 21:33:20 MST 2014 version=1.0
Gene Set	AUC	p-value	Rank	FDR
Signal2	0.545152	0.08	1	0.786667
Signal10	0.522192	0.24	2	0.786667
Signal1	0.513312	0.28	3	0.786667
Random10	0.506128	0.32	4	0.786667
Random2	0.506968	0.36	5	0.786667
Random1	0.490536	0.48	6	0.786667
Mixed2	0.496144	0.50	7	0.786667
Random50	0.487984	0.57	8	0.786667
Random100	0.489984	0.59	9	0.786667
Mixed100	0.472256	0.84	10	0.938182
Mixed10	0.465632	0.86	11	0.938182
Mixed50	0.453872	0.94	12	0.940000
