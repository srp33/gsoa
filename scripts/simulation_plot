#!/bin/bash

set -o errexit

currentDir=$(pwd)

###############################################
# This script plots prior results for all methods.
# The prior results files are listed in Simulation/PriorResults.
# If you are rerunning it, change the value of the resultsDir variable below.
# Note that due to random variation, the results may change slightly from one run to another.
###############################################

resultsDir=Simulation/PriorResults
#resultsDir=Simulation/

for x in Main Unbalanced
do
  # GSOA
  resultsFile=$currentDir/Simulation/GSOA_Results_${x}_svmrbf_1.0_0.0.txt
  Rscript --vanilla code/PlotGsoaSimulatedResults.R $resultsDir/GSOA_Results_${x}_svmrbf_1.0_0.0.txt "GSOA (SVM)" Simulation/GSOA_Results_${x}_svmrbf_1.0_0.0_metric.pdf

  # GSEA
  Rscript --vanilla code/PlotGseaSimulatedResults.R $resultsDir/gsea_report_for_Class?_${x}.txt "GSEA" Simulation/GSEA_Results_${x}_metric.pdf

  # GSAA
  Rscript --vanilla code/PlotGseaSimulatedResults.R $resultsDir/gsaa_report_for_Class?_${x}.txt "GSAA" Simulation/GSAA_Results_${x}_metric.pdf

  # GAGE
  Rscript --vanilla code/PlotGageSimulatedResults.R $resultsDir/GAGE_${x}_tstat.txt "GAGE (tstat)" Simulation/GAGE_${x}_tstats_metric.pdf
  # Note: We tried GAGE with FC, but the tstat result was much better, so we focused on that.

  # Compare across methods
  Rscript --vanilla code/PlotSensSpec.R $resultsDir/GSOA_Results_${x}_svmrbf_1.0_0.0${algorithm}.txt $resultsDir/gsea_report_for_Class?_${x}.txt $resultsDir/gsaa_report_for_Class?_${x}.txt $resultsDir/GAGE_${x}_tstat.txt FDR 0.2 Simulation/${x}_Sensitivity_Comparison_FDR_0.2_svmrbf_1.0_0.0${algorithm}.pdf Simulation/${x}_Specificity_Comparison_FDR_0.2_svmrbf_1.0_0.0${algorithm}.pdf Simulation/${x}_Accuracy_Comparison_FDR_0.2_svmrbf_1.0_0.0${algorithm}.pdf
  Rscript --vanilla code/PlotSensSpec.R $resultsDir/GSOA_Results_${x}_svmrbf_1.0_0.0${algorithm}.txt $resultsDir/gsea_report_for_Class?_${x}.txt $resultsDir/gsaa_report_for_Class?_${x}.txt $resultsDir/GAGE_${x}_tstat.txt pvalue 0.05 Simulation/${x}_Sensitivity_Comparison_pvalue_0.05_svmrbf_1.0_0.0${algorithm}.pdf Simulation/${x}_Specificity_Comparison_pvalue_0.05_svmrbf_1.0_0.0${algorithm}.pdf Simulation/${x}_Accuracy_Comparison_pvalue_0.05_svmrbf_1.0_0.0${algorithm}.pdf
done

###############################################
# Crop PDFs (optional)
###############################################

for f in Simulation/*pdf
do
  pdfcrop --margins=2 $f $f &
done
wait
